#!/usr/bin/env python
"""
Create desktop shortcuts for Windows
"""
import os

import win32com.client
from win32com.shell import shell, shellcon

from . import UserFolders
from .shortcut import shortcut

scut_ext = "lnk"
ico_ext = ("ico",)

_WSHELL = win32com.client.Dispatch("Wscript.Shell")


# Windows Special Folders
# see: https://docs.microsoft.com/en-us/windows/win32/shell/csidl


def get_homedir():
    """Return home directory:
    note that we return CSIDL_PROFILE, not
    CSIDL_APPDATA, CSIDL_LOCAL_APPDATA,  or CSIDL_COMMON_APPDATA
    """
    return shell.SHGetFolderPath(0, shellcon.CSIDL_PROFILE, None, 0)


def get_desktop():
    """Return user Desktop folder"""
    return shell.SHGetFolderPath(0, shellcon.CSIDL_DESKTOP, None, 0)


def get_startmenu():
    """Return user Start Menu Programs folder
    note that we return CSIDL_PROGRAMS not CSIDL_COMMON_PROGRAMS
    """
    return shell.SHGetFolderPath(0, shellcon.CSIDL_PROGRAMS, None, 0)


def get_folders():
    """get user-specific folders

    Returns:
    -------
    Named tuple with fields 'home', 'desktop', 'startmenu'

    Example:
    -------
    >>> from pyshortcuts import get_folders
    >>> folders = get_folders()
    >>> print("Home, Desktop, StartMenu ",
    ...       folders.home, folders.desktop, folders.startmenu)
    """
    return UserFolders(get_homedir(), get_desktop(), get_startmenu())


def make_shortcut(
    script,
    name=None,
    description=None,
    icon=None,
    folder=None,
    terminal=True,
    desktop=True,
    startmenu=True,
    executable=None,
):
    """create shortcut

    Arguments:
    ---------
    script      (str) path to script, may include command-line arguments
    name        (str, None) name to display for shortcut [name of script]
    description (str, None) longer description of script [`name`]
    icon        (str, None) path to icon file [python icon]
    folder      (str, None) subfolder of Desktop for shortcut [None] (See Note 1)
    terminal    (bool) whether to run in a Terminal [True]
    desktop     (bool) whether to add shortcut to Desktop [True]
    startmenu   (bool) whether to add shortcut to Start Menu [True] (See Note 2)
    executable  (str, None) name of executable to use [this Python] (see Note 3)

    Notes:
    ------
    1. `folder` will place shortcut in a subfolder of Desktop and/or Start Menu
    2. Start Menu does not exist for Darwin / MacOSX
    3. executable defaults to the Python executable used to make shortcut.
    """
    userfolders = get_folders()

    scut = shortcut(
        script,
        userfolders,
        name=name,
        description=description,
        folder=folder,
        icon=icon,
    )

    for create, folder in (
        (desktop, scut.desktop_dir),
        (startmenu, scut.startmenu_dir),
    ):
        if create:
            if not os.path.exists(folder):
                os.makedirs(folder)
            dest = os.path.normpath(os.path.join(folder, scut.target))

            wscript = _WSHELL.CreateShortCut(dest)
            wscript.Targetpath = '"%s"' % executable
            # "/C" means run and terminate. otherwise, the cmd will interpret the script as a parameter, which does not exist, and just launches a cmd
            wscript.Arguments = "/C " + script
            wscript.WorkingDirectory = userfolders.home
            wscript.WindowStyle = 7  # 7 means Minimized window at startup
            wscript.Description = scut.description
            wscript.IconLocation = scut.icon
            wscript.save()

    return scut
